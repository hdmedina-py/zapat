<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ArticuloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articulos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Articulo'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'tipo_articulo',
                'value' => 'tipo.descripcion',
                'label' => 'Tipo de Articulo'
            ],
            [
                'attribute' => 'marca',
                'value' => 'marca.descripcion',
                'label' => 'Marca'
            ],
            [
                'attribute' => 'color',
                'value' => 'color.descripcion',
                'label' => 'Color'
            ],
            [
                'attribute' => 'club',
                'value' => 'club.descripcion',
                'label' => 'Club'
            ],
            'numero',
            'talle',
            'precio_costo',
            'precio_venta',
            'cantidad',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

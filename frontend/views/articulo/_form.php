<?php

use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Articulo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articulo-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [
            'tipo_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $model->tipos,
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                    ]
                ],
            ],
            'marca_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $model->marcas,
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                    ]
                ],
            ],
            'talle' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Talle']
            ],
        ]
    ]) ?>
    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'color_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $model->colores,
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                    ]
                ],
            ],
            'club_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $model->clubes,
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                    ]
                ]
            ],
        ]
    ]) ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'numero' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Numero']
            ],
            'cantidad' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Cantidad']
            ],
        ]
    ]) ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'precio_costo' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Precio de Costo']
            ],
            'precio_venta' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder' => 'Precio de Venta']
            ],
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

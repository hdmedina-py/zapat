<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin
    ([
        'brandLabel' => $this->title,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        $menuItems = [
            ['label' => 'Inicio', 'url' => ['/site/index']],
            ['label' => 'Articulo', 'url' => ['/articulo/index']],
            ['label' => 'Venta Articulo', 'url' => ['/articulo/venta']],
            ['label' => 'Club', 'url' => ['/club/index']],
            ['label' => 'Color', 'url' => ['/color/index']],
            ['label' => 'Marca', 'url' => ['/marca/index']],
            ['label' => 'Tipo Articulo', 'url' => ['/tipo-articulo/index']],
            ['label' => 'Salir', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],

        ];

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
    }
    //    if (Yii::$app->user->isGuest) {
    //        $menuItems[] = ['label' => 'Registrarse', 'url' => ['/site/signup']];
    //        $menuItems[] = ['label' => 'Iniciar Sesion', 'url' => ['/site/login']];
    //    } else {
    //        $menuItems[] = ['label' => '|  Hola ' . Yii::$app->user->identity->username, 'items' => [
    //
    //            ['label' => 'Perfil', 'url' => ['/perfil/view']],
    //            ['label' => 'Administración', 'url' => ['/site/index']],
    //            ['label' => 'Salir', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
    //        ]];
    //    }

    NavBar::end();
    ?>

    <div class="container">
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= $this->title; ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

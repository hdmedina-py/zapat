<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Venta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="venta-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--    <? //= $form->field($model, 'articulo_id')->textInput(['disabled' => true]) ?>-->
    <?= $form->field($articuloDescripcion, 'descripcion')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'cantidad')->textInput(['onchange' => 'js:cant = $(this).val(); 
    pu = document.getElementById("venta-precio_unitario").value; 
    document.getElementById("venta-precio_total").value = pu*cant; 
    ']) ?>

    <?= $form->field($model, 'precio_unitario')->textInput(['maxlength' => true,
    'onchange' => 'js:pu = $(this).val();
    cant = document.getElementById("venta-cantidad").value;
    document.getElementById("venta-precio_total").value = pu*cant; '
    ]) ?>

    <?= $form->field($model, 'precio_total')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <div style='display:none;'>
        <?php
        echo $form->field($model, 'precio_total')->textInput(['maxlength' => true])->label('');?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'OK') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancelar'), ['articulo/venta'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Venta */

$this->title = Yii::t('app', 'Create Venta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ventas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'articuloDescripcion' => $articuloDescripcion
    ]) ?>

</div>

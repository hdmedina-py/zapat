<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Articulo;

/**
 * ArticuloSearch represents the model behind the search form about `frontend\models\Articulo`.
 */
class ArticuloSearch extends Articulo
{
    public $tipo_articulo;
    public $marca;
    public $color;
    public $club;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_id', 'marca_id', 'color_id', 'club_id', 'numero', 'precio_costo', 'precio_venta', 'cantidad'], 'integer'],
            [['created_at', 'updated_at', 'tipo_articulo', 'marca', 'color', 'club'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Articulo::find();
        $query->joinWith(['tipo as ta'])
        ->joinWith(['marca as m'])
        ->joinWith(['color as c'])
        ->joinWith(['club as cl']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tipo_articulo'] = [
            'asc' => ['ta.descripcion' => SORT_ASC],
            'desc' => ['ta.descripcion' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['marca'] = [
            'asc' => ['m.descripcion' => SORT_ASC],
            'desc' => ['m.descripcion' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['color'] = [
            'asc' => ['c.descripcion' => SORT_ASC],
            'desc' => ['c.descripcion' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['club'] = [
            'asc' => ['cl.descripcion' => SORT_ASC],
            'desc' => ['cl.descripcion' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_id' => $this->tipo_id,
            'marca_id' => $this->marca_id,
            'color_id' => $this->color_id,
            'club_id' => $this->club_id,
            'numero' => $this->numero,
            'precio_costo' => $this->precio_costo,
            'precio_venta' => $this->precio_venta,
            'cantidad' => $this->cantidad,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ta.descripcion', $this->tipo_articulo]);
        $query->andFilterWhere(['like', 'm.descripcion', $this->marca]);
        $query->andFilterWhere(['like', 'c.descripcion', $this->color]);
        $query->andFilterWhere(['like', 'cl.descripcion', $this->club]);

        return $dataProvider;
    }
}

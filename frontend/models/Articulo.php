<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "articulo".
 *
 * @property integer $id
 * @property integer $tipo_id
 * @property integer $marca_id
 * @property integer $color_id
 * @property integer $club_id
 * @property integer $numero
 * @property string $talle
 * @property integer $precio_costo
 * @property integer $precio_venta
 * @property integer $cantidad
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Club $club
 * @property Color $color
 * @property Marca $marca
 * @property Tipo $tipo
 */
class Articulo extends \yii\db\ActiveRecord
{
    /**
     * behaviors
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articulo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_id', 'precio_costo', 'precio_venta', 'cantidad', 'marca_id', 'color_id'], 'required'],
            [['tipo_id', 'marca_id', 'color_id', 'club_id', 'numero', 'cantidad'], 'integer'],
            [['talle'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Club::className(), 'targetAttribute' => ['club_id' => 'id']],
            [['color_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color_id' => 'id']],
            [['marca_id'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_id' => 'id']],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_id' => 'id']],
            [['tipo_id', 'marca_id', 'color_id', 'numero'], 'unique', 'attributes' => ['tipo_id', 'marca_id', 'color_id', 'numero']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tipo_id' => Yii::t('app', 'Tipo'),
            'marca_id' => Yii::t('app', 'Marca'),
            'color_id' => Yii::t('app', 'Color'),
            'club_id' => Yii::t('app', 'Club'),
            'numero' => Yii::t('app', 'Numero'),
            'precio_costo' => Yii::t('app', 'Precio de Costo'),
            'precio_venta' => Yii::t('app', 'Precio de Venta'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Club::className(), ['id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(Marca::className(), ['id' => 'marca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_id']);
    }

    public function getTipos()
    {
        $droptions = Tipo::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'descripcion');
    }

    public function getMarcas()
    {
        $droptions = Marca::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'descripcion');
    }

    public function getColores()
    {
        $droptions = Color::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'descripcion');
    }

    public function getClubes()
    {
        $droptions = Club::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'descripcion');
    }
}

<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "venta".
 *
 * @property integer $id
 * @property integer $articulo_id
 * @property integer $cantidad
 * @property string $precio_unitario
 * @property string $precio_total
 * @property string $created_at
 * @property string $updated_at
 */
class Venta extends \yii\db\ActiveRecord
{
    /**
     * behaviors
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'venta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articulo_id', 'precio_unitario', 'precio_total'], 'required'],
            [['articulo_id', 'cantidad'], 'integer'],
            [['precio_unitario', 'precio_total'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'articulo_id' => Yii::t('app', 'Articulo'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'precio_venta' => Yii::t('app', 'Precio Venta'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}

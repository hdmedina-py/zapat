<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "color".
 *
 * @property integer $id
 * @property string $descripcion
 *
 * @property Articulo[] $articulos
 */
class Color extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion',], 'required'],
            [['descripcion',], 'string', 'max' => 45],
            [['descripcion',], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticulos()
    {
        return $this->hasMany(Articulo::className(), ['color_id' => 'id']);
    }
}

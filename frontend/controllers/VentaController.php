<?php

namespace frontend\controllers;

use frontend\models\Articulo;
use Yii;
use frontend\models\Venta;
use frontend\models\search\Venta as VentaSearch;
use yii\base\Exception;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class VentaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Venta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Venta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Venta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idArticulo)
    {
        $model = new Venta();
        $articulo = Articulo::find()
            ->where(['id' => $idArticulo])
            ->one();
        $model->articulo_id = $idArticulo;

        if ($model->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            $model->cantidad = intval($model->cantidad);

            try {
                if ($model->validate() && $model->cantidad <= $articulo->cantidad) {
                    $model->save();
                } else {
                    throw new Exception();
                }

                //descontamos
                $articulo->cantidad = $articulo->cantidad - $model->cantidad;
                if ($articulo->validate()) {
                    $articulo->save();
                } else {
                    throw new Exception();
                }

                $transaction->commit();
            } catch (Exception $exception) {
                $transaction->rollBack();
            }
            return $this->redirect(['articulo/venta']);
        } else {

            $articuloDescripcion = new DescripcionArticulo();
            $articuloDescripcion->descripcion =
                $articulo->tipo->descripcion . ' ' .
                $articulo->marca->descripcion . ' ' .
                $articulo->color->descripcion . ' ' .
                $articulo->numero;

            $model->precio_unitario = $articulo->precio_venta;
            $model->precio_total = $articulo->precio_venta;
            $model->cantidad = 1;

            return $this->render('create', [
                'model' => $model,
                'articuloDescripcion' => $articuloDescripcion
            ]);
        }
    }

    /**
     * Updates an existing Venta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Venta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Venta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Venta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Venta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

class DescripcionArticulo extends Model
{
    public $descripcion;
}
<?php

use jamband\schemadump\Migration;

class m170625_012216_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
// articulo
        $this->createTable('{{%articulo}}', [
            'id' => $this->primaryKey(),
            'tipo_id' => $this->integer(11)->notNull(),
            'marca_id' => $this->integer(11)->notNull(),
            'color_id' => $this->integer(11)->notNull(),
            'club_id' => $this->integer(11)->null(),
            'numero' => $this->integer(11)->null(),
            'talle' => $this->string(10)->null(),
            'precio_costo' => $this->decimal(12)->notNull(),
            'precio_venta' => $this->decimal(12)->notNull(),
            'cantidad' => $this->integer(11)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
        ], $this->tableOptions);

// club
        $this->createTable('{{%club}}', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(45)->notNull()->unique(),
        ], $this->tableOptions);

// color
        $this->createTable('{{%color}}', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(45)->notNull()->unique(),
            'sigla' => $this->string(10)->notNull()->unique(),
        ], $this->tableOptions);

// estado
        $this->createTable('{{%estado}}', [
            'id' => $this->primaryKey(),
            'estado_nombre' => $this->string(45)->notNull(),
            'estado_valor' => $this->smallInteger(6)->notNull(),
        ], $this->tableOptions);

// genero
        $this->createTable('{{%genero}}', [
            'id' => $this->primaryKey()->unsigned(),
            'genero_nombre' => $this->string(45)->notNull(),
        ], $this->tableOptions);

// marca
        $this->createTable('{{%marca}}', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(45)->notNull()->unique(),
        ], $this->tableOptions);

// perfil
        $this->createTable('{{%perfil}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer(10)->unsigned()->notNull(),
            'nombre' => $this->text()->null(),
            'apellido' => $this->text()->null(),
            'fecha_nacimiento' => $this->datetime()->null(),
            'genero_id' => $this->integer(10)->unsigned()->notNull(),
            'created_at' => $this->datetime()->null(),
            'updated_at' => $this->datetime()->null(),
        ], $this->tableOptions);

// rol
        $this->createTable('{{%rol}}', [
            'id' => $this->primaryKey(),
            'rol_nombre' => $this->string(45)->notNull(),
            'rol_valor' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

// tipo
        $this->createTable('{{%tipo}}', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(45)->notNull()->unique(),
            'sigla' => $this->string(10)->notNull()->unique(),
        ], $this->tableOptions);

// tipo_usuario
        $this->createTable('{{%tipo_usuario}}', [
            'id' => $this->primaryKey(),
            'tipo_usuario_nombre' => $this->string(45)->notNull(),
            'tipo_usuario_valor' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

// user
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string(255)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255)->null()->unique(),
            'email' => $this->string(255)->notNull()->unique(),
            'tipo_usuario_id' => $this->integer(11)->notNull()->defaultValue(1),
            'estado_id' => $this->integer(11)->notNull()->defaultValue(1),
            'rol_id' => $this->integer(11)->notNull()->defaultValue(1),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
        ], $this->tableOptions);

// fk: articulo
        $this->addForeignKey('fk_articulo_club_id', '{{%articulo}}', 'club_id', '{{%club}}', 'id');
        $this->addForeignKey('fk_articulo_color_id', '{{%articulo}}', 'color_id', '{{%color}}', 'id');
        $this->addForeignKey('fk_articulo_marca_id', '{{%articulo}}', 'marca_id', '{{%marca}}', 'id');
        $this->addForeignKey('fk_articulo_tipo_id', '{{%articulo}}', 'tipo_id', '{{%tipo}}', 'id');

// fk: perfil
        $this->addForeignKey('fk_perfil_genero_id', '{{%perfil}}', 'genero_id', '{{%genero}}', 'id');
        $this->addForeignKey('fk_perfil_user_id', '{{%perfil}}', 'user_id', '{{%user}}', 'id');

// fk: user
        $this->addForeignKey('fk_user_estado_id', '{{%user}}', 'estado_id', '{{%estado}}', 'id');
        $this->addForeignKey('fk_user_rol_id', '{{%user}}', 'rol_id', '{{%rol}}', 'id');
        $this->addForeignKey('fk_user_tipo_usuario_id', '{{%user}}', 'tipo_usuario_id', '{{%tipo_usuario}}', 'id');

        //insert data
        //Estado
        $this->insert('estado', array(
            'estado_nombre' => 'Activo',
            'estado_valor' => 10,
        ));
        $this->insert('estado', array(
            'estado_nombre' => 'Pendiente',
            'estado_valor' => 5,
        ));
        $this->insert('estado', array(
            'estado_nombre' => 'Inactivo',
            'estado_valor' => 0,
        ));

        //Genero
        $this->insert('genero', array(
            'genero_nombre' => 'Masculino',
        ));
        $this->insert('genero', array(
            'genero_nombre' => 'Femenino',
        ));

        //Rol
        $this->insert('rol', array(
            'rol_nombre' => 'Usuario',
            'rol_valor' => 10,
        ));
        $this->insert('rol', array(
            'rol_nombre' => 'Admin',
            'rol_valor' => 20,
        ));
        $this->insert('rol', array(
            'rol_nombre' => 'SuperUsuario',
            'rol_valor' => 30,
        ));

        //Tipo Usuario
        $this->insert('tipo_usuario', array(
            'tipo_usuario_nombre' => 'normal',
            'tipo_usuario_valor' => 10,
        ));
        $this->insert('tipo_usuario', array(
            'tipo_usuario_nombre' => 'administrador',
            'tipo_usuario_valor' => 30,
        ));

        $this->insert('user', array(
            'username' => 'admin',
            'auth_key' => 'ZcqWkNR0Vom_GQOY9AWnkVK-PnwpRf2w',
            'password_hash' => '$2y$13$IeoeOg.YZsU5jFs35YuPLeNi9w9m7LZ.ke8CvO6aVtG49YIw7CiRO', //.admin.
            'email' => 'admin@ingenio.com.py',
            'rol_id' => 3, //SuperUsuario
            'tipo_usuario_id' => 2,
            'estado_id' => 1, //Activo
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ));
    }

    public function down()
    {
        $this->dropTable('{{%perfil}}'); // fk: genero_id, user_id
        $this->dropTable('{{%estado}}');
        $this->dropTable('{{%genero}}');
        $this->dropTable('{{%rol}}');
        $this->dropTable('{{%tipo_usuario}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%club}}');
        $this->dropTable('{{%marca}}');
        $this->dropTable('{{%tipo}}');
        $this->dropTable('{{%color}}');
        $this->dropTable('{{%articulo}}');
    }
}

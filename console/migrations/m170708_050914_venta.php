<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m170708_050914_venta extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%venta}}', [
            'id' => $this->primaryKey(),
            'articulo_id' => $this->integer(11)->notNull(),
            'cantidad' => $this->integer(11)->null(),
            'precio_unitario' => $this->decimal(12)->notNull(),
            'precio_total' => $this->decimal(12)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%venta}}');
    }
}
